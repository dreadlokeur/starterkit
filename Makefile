#!make

#Default settings
.DEFAULT_GOAL                   := help
DEFAULT_APP_DOMAIN              = starterkit.localhost

#Directories variables
DIR_FRONTOFFICE                 = frontoffice/
DIR_API                         = api/
DIR_DOCKER                      = docker/
DIR_DOCKER_COMPOSE_CONF         = $(DIR_DOCKER)docker-compose/
DIR_DOCKER_PROXY_CERT           = $(DIR_DOCKER)traefik/certs/
#Binaries variables
BIN_DOCKER                      = docker
BIN_AWK                         = awk
BIN_ECHO                        = echo
BIN_MAKE                        = make
BIN_CAT                         = cat
BIN_MV                          = mv
BIN_SED                         = sed
BIN_READ                        = read
BIN_EVAL                        = eval
BIN_TEST                        = test
BIN_CUT                         = cut
BIN_DOCKER_COMPOSE              = @docker-compose
BIN_OPENSSL                     = @openssl
BIN_XDG_OPEN                    = @xdg-open
BIN_CP                          = @cp
BIN_RM                          = @rm

#Includes
ifeq ($(shell $(BIN_TEST) ! -f $(DIR_DOCKER_COMPOSE_CONF).env && $(BIN_ECHO) nok || $(BIN_ECHO) ok),ok)
	include $(DIR_DOCKER_COMPOSE_CONF).env
endif
ifeq ($(shell $(BIN_TEST) ! -f $(DIR_DOCKER_COMPOSE_CONF).env.local && $(BIN_ECHO) nok || $(BIN_ECHO) ok),ok)
	include $(DIR_DOCKER_COMPOSE_CONF).env.local
endif

#Override if dont exist (setting in .env or .env.local)
APP_NAME                        ?= starterkit
APP_ENV                         ?= prod
APP_DOMAIN                      ?= $(DEFAULT_APP_DOMAIN)
APP_CI                          ?= false

#Settings variables
MAKE_OPTS                       = --no-print-directory
DOCKER_COMP_OPTS                = --project-name $(APP_NAME)
DOCKER_COMP_FILES               = -f $(DIR_DOCKER_COMPOSE_CONF)docker-compose.yml
DOCKER_COMP_ENV_FILES           = --env-file $(DIR_DOCKER_COMPOSE_CONF).env
NPROC                           := 1
OS                              := $(shell uname)
UID                             := $(shell id -u)
GID                             := $(shell id -g)
GREEN                           := $(shell tput -Txterm setaf 2)
YELLOW                          := $(shell tput -Txterm setaf 3)
WHITE                           := $(shell tput -Txterm setaf 7)
RED                             := $(shell tput -Txterm setaf 1)
BLUE                            := $(shell tput -Txterm setaf 4)
PURPLE                          := $(shell tput -Txterm setaf 5)
RESET                           := $(shell tput -Txterm sgr 0)
TARGET_MAX_CHAR_NUM             = 30
DOCKER_MIN_VERSION              = 19
CHECK_DOCKER_VERSION            = $(shell if [ `$(BIN_DOCKER) version -f "{{.Server.Version}}" | $(BIN_CUT) -d'.' -f 1` -lt $(DOCKER_MIN_VERSION) ];then $(BIN_ECHO) nok; fi;)
ASCII                           =
VERSION                         =

#Override variables
ifeq ($(OS),Linux)
    NPROC                      := $(shell nproc)
else ifeq ($(OS),Darwin)
    NPROC                      := $(shell sysctl -n hw.ncpu)
endif
ifeq ($(shell $(BIN_TEST) ! -f .version && $(BIN_ECHO) nok || $(BIN_ECHO) ok),ok)
    VERSION                    := $(shell $(BIN_CAT) .version)
endif
ifeq ($(shell $(BIN_TEST) ! -f .ascii && $(BIN_ECHO) nok || $(BIN_ECHO) ok),ok)
		ASCII                      := @$(BIN_CAT) .ascii
endif
ifeq ($(shell ! command -v ccat &> /dev/null && $(BIN_ECHO) nok || $(BIN_ECHO) ok),ok)
		ASCII                      := @ccat -G String="purple" .ascii
endif

ifeq ($(shell $(BIN_TEST) ! -f $(DIR_DOCKER_COMPOSE_CONF)docker-compose.local.yml && $(BIN_ECHO) nok || $(BIN_ECHO) ok),ok)
    DOCKER_COMP_FILES          := $(DOCKER_COMP_FILES) -f $(DIR_DOCKER_COMPOSE_CONF)docker-compose.local.yml
endif
ifeq ($(shell $(BIN_TEST) ! -f $(DIR_DOCKER_COMPOSE_CONF).env.local && $(BIN_ECHO) nok || $(BIN_ECHO) ok),ok)
    DOCKER_COMP_ENV_FILES      := $(DOCKER_COMP_ENV_FILES) --env-file $(DIR_DOCKER_COMPOSE_CONF).env.local
endif

ifeq ($(APP_ENV),"dev")
    DOCKER_COMP_FILES          := $(DOCKER_COMP_FILES) -f $(DIR_DOCKER_COMPOSE_CONF)docker-compose.dev.yml
    ifeq ($(shell $(BIN_TEST) ! -f $(DIR_DOCKER_COMPOSE_CONF)docker-compose.dev.local.yml && $(BIN_ECHO) nok || $(BIN_ECHO) ok),ok)
        DOCKER_COMP_FILES         := $(DOCKER_COMP_FILES) -f $(DIR_DOCKER_COMPOSE_CONF)docker-compose.dev.local.yml
    endif
    DOCKER_COMP_ENV_FILES      := $(DOCKER_COMP_ENV_FILES) --env-file $(DIR_DOCKER_COMPOSE_CONF).env.dev
    ifeq ($(shell $(BIN_TEST) ! -f $(DIR_DOCKER_COMPOSE_CONF).env.dev.local && $(BIN_ECHO) nok || $(BIN_ECHO) ok),ok)
        DOCKER_COMP_ENV_FILES     := $(DOCKER_COMP_ENV_FILES) --env-file $(DIR_DOCKER_COMPOSE_CONF).env.dev.local
    endif
else ifeq ($(APP_ENV),"prod")
    DOCKER_COMP_FILES          := $(DOCKER_COMP_FILES) -f $(DIR_DOCKER_COMPOSE_CONF)docker-compose.prod.yml
    ifeq ($(shell $(BIN_TEST) ! -f $(DIR_DOCKER_COMPOSE_CONF)docker-compose.prod.local.yml && $(BIN_ECHO) nok || $(BIN_ECHO) ok),ok)
        DOCKER_COMP_FILES         := $(DOCKER_COMP_FILES) -f $(DIR_DOCKER_COMPOSE_CONF)docker-compose.prod.local.yml
    endif
    DOCKER_COMP_ENV_FILES      := $(DOCKER_COMP_ENV_FILES) --env-file $(DIR_DOCKER_COMPOSE_CONF).env.prod
    ifeq ($(shell $(BIN_TEST) ! -f $(DIR_DOCKER_COMPOSE_CONF).env.prod.local && $(BIN_ECHO) nok || $(BIN_ECHO) ok),ok)
        DOCKER_COMP_ENV_FILES     := $(DOCKER_COMP_FILES) --env-file $(DIR_DOCKER_COMPOSE_CONF).env.prod.local
    endif
endif
ifeq ($(APP_CI),true)
    DOCKER_COMP_FILES          := $(DOCKER_COMP_FILES) -f $(DIR_DOCKER_COMPOSE_CONF)docker-compose-ci.yml
endif
DOCKER_COMPOSE_FULL            := $(BIN_DOCKER_COMPOSE) $(DOCKER_COMP_OPTS) $(DOCKER_COMP_FILES) $(DOCKER_COMP_ENV_FILES)
MAKE_FULL											 := $(BIN_MAKE) $(MAKE_OPTS)


#Containers variables
CONT_API_HTTPD                 = $(DOCKER_COMPOSE_FULL) run --rm api-httpd
CONT_API_PHP                   = $(DOCKER_COMPOSE_FULL) run --rm api-php
CONT_API_DATABASE              = $(DOCKER_COMPOSE_FULL) run --rm api-database
CONT_FRONTOFFICE_NODE          = $(DOCKER_COMPOSE_FULL) run --rm frontoffice-node
CONT_FRONTOFFICE_HTTPD         = $(DOCKER_COMPOSE_FULL) run --rm frontoffice-httpd
CONT_BACKOFFICE_NODE           = $(DOCKER_COMPOSE_FULL) run --rm backoffice-node
CONT_BACKOFFICE_HTTPD          = $(DOCKER_COMPOSE_FULL) run --rm backoffice-httpd
CONT_PROXY                     = $(DOCKER_COMPOSE_FULL) run --rm reverse-proxy

#GLOBAL messages
define MESSAGE_DOCKER_VERSION_ERROR
${YELLOW}Docker${RESET} version less than ${GREEN}19.0.3${RESET}, can't continue, please upgrade !
endef
define MESSAGE_INSTALLATION_START
${YELLOW}Installation${RESET} starting ...${RESET}
endef
define MESSAGE_INSTALLATION_END
${YELLOW}Installation${RESET} done. You can open the project with make open
endef

## Display help for project
help:
	$(ASCII)
	@$(BIN_ECHO) ""
	@$(BIN_ECHO) "${YELLOW}DOMAIN:${RESET} ${GREEN}https://$(APP_DOMAIN)${RESET} ${YELLOW}ENV:${RESET} ${GREEN}$(APP_ENV)${RESET} ${YELLOW}VERSION:${RESET} ${GREEN}$(VERSION)${RESET}  ${YELLOW}CI:${RESET} ${GREEN}$(APP_CI)${RESET}"
	@$(BIN_ECHO) ""
	@$(BIN_AWK) '/^[a-zA-Z\-_0-9]+:/ { \
			helpMessage = match(lastLine, /^## (.*)/); \
			if (helpMessage) { \
				helpCommand = substr($$1, 0, index($$1, ":")); helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
				printf "  ${GREEN}%-$(TARGET_MAX_CHAR_NUM)s${RESET} ${WHITE}%s${RESET}\n", helpCommand, helpMessage; \
			} \
			isTopic = match(lastLine, /^###/); \
	    if (isTopic) { \
	    	printf "\n${YELLOW}%s\n", $$1;\
    	} \
	} { lastLine = $$0 }' $(MAKEFILE_LIST)



#################################
Project:

## Build containers
build: check-docker
	$(DOCKER_COMPOSE_FULL) build --parallel;

## Rebuild (without cache) containers
rebuild: check-docker
	$(DOCKER_COMPOSE_FULL) build --parallel --no-cache;

## Install environment from scratch
install:
	@$(BIN_ECHO) "$(MESSAGE_INSTALLATION_START)"
	@$(MAKE_FULL) pre-install
	@$(MAKE_FULL) post-install
	@$(BIN_ECHO) "$(MESSAGE_INSTALLATION_END)"

## Pre-install (copy env dist file and check docker requirement)
pre-install:
	@$(MAKE_FULL) check-docker
	@if [ "$(APP_CI)" == false ]; then \
  		$(MAKE_FULL) env-install; \
  fi;


## Setup env (.env.local config), hosts definition and ssl-generation
env-install:
#todo: check if $$env == dev|prod and fix it if APP_ENV= if edited
#	@($(BIN_READ) -p "Please, define your $(YELLOW)APP_ENV$(RESET) $(GREEN)[dev/prod]$(RESET) : " env && case "$$env" in "") \
# 		true;; *) \
# 			$(BIN_CAT) .env.local | $(BIN_SED) -e "s#APP_ENV=\"dev\"#APP_ENV=\"$$env\"#g" > .env.local.tmp && $(BIN_MV) .env.local.tmp .env.local;; \
#	esac)
#todo check if $domain is valid domain X.X format
#	@($(BIN_READ) -p "Please, define your $(YELLOW)APP_DOMAIN$(RESET) $(GREEN)[example : $(DEFAULT_APP_DOMAIN)] $(RESET): " domain && case "$$domain" in "") \
#  		true;; *) \
# 			$(BIN_CAT) .env.local | $(BIN_SED) -e "s#APP_DOMAIN=\"$(DEFAULT_APP_DOMAIN)\"#APP_DOMAIN=\"$$domain\"#g" > .env.local.tmp && $(BIN_MV) .env.local.tmp .env.local;; \
# 	esac)
#todo must be completed with APP_DOMAIN
#@( $(BIN_READ) -p "Add your $(YELLOW)APP_DOMAIN$(RESET) on your /etc/hosts file ? $(GREEN)[y/N]$(RESET): " sure && case "$$sure" in [yY]) echo "127.0.0.1 *.$(APP_DOMAIN)" | sudo tee -a /etc/hosts && true;; *);; esac )
#todo app_name
#todo .env on docker-compose config
#todo parameter for no prompt

## Post-install (build docker images, start environnement and install vendors packages)
post-install:
	@$(BIN_ECHO) "Your env setting is : $(GREEN)$(APP_ENV)$(RESET)";
	@$(BIN_ECHO) "Your domain setting is : $(GREEN)$(APP_DOMAIN)$(RESET)";
	@if [ "$(APP_CI)" == false ]; then \
  		if [ "$(APP_ENV)" == "dev" ];then \
  		  	$(BIN_READ) -p "Do you want generate SSL certificates for traefik ? $(GREEN)[y/N]$(RESET): " sure && case "$$sure" in [yY]) $(MAKE_FULL) ssl-generate && true;; *);; esac; \
  		fi; \
  fi;
	@$(MAKE_FULL) build
	@$(MAKE_FULL) start
	@$(MAKE_FULL) env-local
	@$(MAKE_FULL) install-assets
	@$(MAKE_FULL) build-assets

## Install assets (composer and node packages)
install-assets: permfix clean-assets
	@if [ "$(APP_ENV)" == "dev" ]; then \
  		$(MAKE_FULL) api-install-dev && $(MAKE_FULL) frontoffice-install-dev; \
  fi;
	@if [ "$(APP_ENV)" == "prod" ]; then \
  		$(MAKE_FULL) api-install && $(MAKE_FULL) frontoffice-install; \
  fi;

## Builds sources assets (node compilation build)
build-assets:
	@if [ "$(APP_ENV)" == "dev" ]; then \
  		$(MAKE_FULL) frontoffice-yarn-build-dev; \
  fi;
	@if [ "$(APP_ENV)" == "prod" ]; then \
  		$(MAKE_FULL) frontoffice-yarn-build; \
  fi;

## Builds sources assets (rebuilding on file changes) (node compilation build)
watch-assets:
	@if [ "$(APP_ENV)" == "dev" ]; then \
  		$(MAKE_FULL) frontoffice-yarn-watch-dev; \
  fi;
	@if [ "$(APP_ENV)" == "prod" ]; then \
  		$(MAKE_FULL) frontoffice-yarn-watch; \
  fi;
#todo fix no quit possible

## Cleaning previous install assets
clean-assets: api-clean frontoffice-clean

## Cleaning previous install env
clean:
	@($(BIN_READ) -p "Cleanings assets previous install ? $(GREEN)[y/N]$(RESET): " sure && case "$$sure" in [yY]) $(MAKE_FULL) clean-assets;; esac)
	@$(MAKE_FULL) confirm
	$(BIN_RM) -Rf $(DIR_DOCKER_COMPOSE_CONF).env.local
	$(BIN_RM) -Rf $(DIR_DOCKER_COMPOSE_CONF).env.dev.local
	$(BIN_RM) -Rf $(DIR_DOCKER_COMPOSE_CONF).env.prod.local
	$(BIN_RM) -Rf $(DIR_DOCKER_COMPOSE_CONF)docker-compose.local.yml
	$(BIN_RM) -Rf $(DIR_DOCKER_COMPOSE_CONF)docker-compose.dev.local.yml
	$(BIN_RM) -Rf $(DIR_DOCKER_COMPOSE_CONF)docker-compose.prod.local.yml

## Display logs stream for all or c="name" containers
logs: check-docker
	@$($(BIN_EVAL) c ?=)
	$(DOCKER_COMPOSE_FULL) logs -f --tail=0 $(c)

## Start all or c="name" containers in backgound
start: check-docker
	@$($(BIN_EVAL) c ?=)
	$(DOCKER_COMPOSE_FULL) up -d --remove-orphans --force-recreate $(c)

## Start all or c="name" containers in foreground
start-dev: check-docker
	@$($(BIN_EVAL) c ?=)
	$(DOCKER_COMPOSE_FULL) up --remove-orphans --force-recreate $(c)

## Stop all or c="name" containers
stop: check-docker
	@$($(BIN_EVAL) c ?=)
	$(DOCKER_COMPOSE_FULL) stop $(c)

## Stop and remove containers, networks and volumes all or c="name" containers
down: check-docker confirm
	@$($(BIN_EVAL) c ?=)
	$(DOCKER_COMPOSE_FULL) down -v --remove-orphans $(c)

## Pause all or c="name" containers
pause: check-docker
	@$($(BIN_EVAL) c ?=)
	$(DOCKER_COMPOSE_FULL) pause $(c)

## Unpause all or c="name" containers
unpause: check-docker
	@$($(BIN_EVAL) c ?=)
	$(DOCKER_COMPOSE_FULL) unpause $(c)

## Show status of all containers or c="name" containers
status: check-docker
	@$($(BIN_EVAL) c ?=)
	$(DOCKER_COMPOSE_FULL) ps $(c)

## Show container IPs.
ips:
	@for CONTAINER in $$($(BIN_DOCKER) container ls --filter name=$(APP_NAME) | $(BIN_AWK) '{print $$NF}' | $(BIN_AWK) 'NR > 1'); \
			do $(BIN_ECHO) "$(GREEN)$$CONTAINER$(RESET): " $(WHITE)$$($(BIN_DOCKER) inspect --format '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $$CONTAINER);  \
	done | column -t

## Restart all or c="name" containers
restart: check-docker
	@$($(BIN_EVAL) c ?=)
	$(DOCKER_COMPOSE_FULL) restart $(c);

## Show docker-compose config, pass the parameter "o=" for option, make config o="dry-run"
config: check-docker
	@$($(BIN_EVAL) o ?=)
	$(DOCKER_COMPOSE_FULL) config --$(o);

## Open project in browser
open:
	$(BIN_XDG_OPEN) https://$(APP_DOMAIN)
	$(BIN_XDG_OPEN) https://$(SUB_DOMAIN_ADMIN).$(APP_DOMAIN)
	$(BIN_XDG_OPEN) https://$(SUB_DOMAIN_API).$(APP_DOMAIN)
	$(BIN_XDG_OPEN) https://traefik.$(APP_DOMAIN)



#################################
Helpers:

## Check docker minimum version, and configuration settings
check-docker:
ifeq ($(CHECK_DOCKER_VERSION),nok)
	$(error $(MESSAGE_DOCKER_VERSION_ERROR))
endif
ifeq ($(shell $(BIN_TEST) ! -f $(DIR_DOCKER_COMPOSE_CONF).env.local && $(BIN_ECHO) nok || $(BIN_ECHO) ok),nok)
	$(BIN_CP) $(DIR_DOCKER_COMPOSE_CONF).env.local.dist $(DIR_DOCKER_COMPOSE_CONF).env.local
endif
ifeq ($(shell $(BIN_TEST) ! -f $(DIR_DOCKER_COMPOSE_CONF)docker-compose.local.yml && $(BIN_ECHO) nok || $(BIN_ECHO) ok),nok)
	$(BIN_CP) $(DIR_DOCKER_COMPOSE_CONF)docker-compose.local.yml.dist $(DIR_DOCKER_COMPOSE_CONF)docker-compose.local.yml
endif
	$(BIN_CP) -au $(DIR_DOCKER_COMPOSE_CONF)docker-compose.dev.local.yml.dist $(DIR_DOCKER_COMPOSE_CONF)docker-compose.dev.local.yml;
	$(BIN_CP) -au $(DIR_DOCKER_COMPOSE_CONF).env.dev.local.dist $(DIR_DOCKER_COMPOSE_CONF).env.dev.local;
	$(BIN_CP) -au $(DIR_DOCKER_COMPOSE_CONF)docker-compose.prod.local.yml.dist $(DIR_DOCKER_COMPOSE_CONF)docker-compose.prod.local.yml;
	$(BIN_CP) -au $(DIR_DOCKER_COMPOSE_CONF).env.prod.local.dist $(DIR_DOCKER_COMPOSE_CONF).env.prod.local;

## Generate ssl certificats traefik reverse-proxy
ssl-generate:
	$(BIN_OPENSSL) req -newkey rsa:2048 -new -nodes -x509 -days 3650 -keyout $(DIR_DOCKER_PROXY_CERT)key.pem -out $(DIR_DOCKER_PROXY_CERT)cert.pem

## Setup env files : copy .env.local.dist .env.local if not exist
env-local: api-env-local

## Confirm helper
confirm:
	@($(BIN_READ) -p "Really, are you sure ? $(GREEN)[y/N]$(RESET): " sure && case "$$sure" in [yY]) true;; *) false;; esac)

## Show ENV variables in .env files
show-env:
	@$(BIN_CAT) .env
	@$(BIN_CAT) .env.local

## Show env APP_ENV
show-env-app_env:
	@$(BIN_ECHO) "$(APP_ENV)"

## Show env APP_DOMAIN
show-env-app_domain:
	@$(BIN_ECHO) "$(APP_DOMAIN)"

## Show env APP_CI
show-env-app_ci:
	@$(BIN_ECHO) "$(APP_CI)"

## Fix linux permissions
permfix: api-permfix frontoffice-permfix

## Clear the cache, pass the parameter p="dev" for override env, example: make cache:clear p="dev"
cache-clear:
	$($(BIN_EVAL) p ?=)
	@$(MAKE_FULL) api-cache-clear p="${or ${p}, $(APP_ENV)}"

## Clear the cache warmup, pass the parameter p="dev" for override env, example: make cache:warmup p="dev"
cache-warmup:
	$($(BIN_EVAL) p ?=)
	@$(MAKE_FULL) api-cache-warmup p="${or ${p}, $(APP_ENV)}"

## Connect to a container, example: make container c="api-php"
container:
	@$($(BIN_EVAL) c ?=)
	$(DOCKER_COMPOSE_FULL) run --rm $(c) sh

#################################
Api:

## Display help for api
api-help:
	$(CONT_API_PHP) $(MAKE_FULL) APP_ENV=$(APP_ENV) APP_CI=$(APP_CI) APP_DOMAIN=$(APP_DOMAIN) SUB_DOMAIN_ADMIN=$(SUB_DOMAIN_ADMIN) help

## Run in command in api, pass the parameter with p="name"
api-%:
	@$($(BIN_EVAL) p ?=)
	$(CONT_API_PHP) $(MAKE_FULL) APP_ENV=$(APP_ENV) APP_CI=$(APP_CI) $* p="$(p)"

## Connect to api php container
api-sh:
	$(CONT_API_PHP) sh

## Run symfony console in api container, pass the parameter c="name" to run a given command, example: make api-console c="about"
api-console:
	@$($(BIN_EVAL) c ?=)
	$(CONT_API_PHP) $(MAKE_FULL) APP_ENV=$(APP_ENV) APP_CI=$(APP_CI) console c="$(c)"

## Run composer command in api container, example: make api-composer c="req symfony/orm-pack"
api-composer:
	@$($(BIN_EVAL) c ?=)
	$(CONT_API_PHP) $(MAKE_FULL) APP_ENV=$(APP_ENV) APP_CI=$(APP_CI) composer c="$(c)"

## Fix linux permissions into api container
api-permfix:
	$(CONT_API_PHP) $(MAKE_FULL) APP_ENV=$(APP_ENV) APP_CI=$(APP_CI) permfix uid=$(UID) gid=$(GID)

## Setup env files : copy .env.local.dist .env.local into api container
api-env-local:
	$(CONT_API_PHP) $(MAKE_FULL) APP_ENV=$(APP_ENV) APP_CI=$(APP_CI) env-local

## Cleaning previous api install
api-clean:
	$(CONT_API_PHP) $(MAKE_FULL) APP_ENV=$(APP_ENV) APP_CI=$(APP_CI) clean

## Install api environment from scratch
api-install: api-vendor-clearcache api-vendor
	@$(BIN_ECHO) "${GREEN}API composer vendors installed${RESET}"

## Install api environment from scratch with debug and tools
api-install-dev: api-install api-tools-install
	@$(BIN_ECHO) "${GREEN}API composer vendors tools installed${RESET}"



#################################
Frontoffice:

## Display help for frontoffice
frontoffice-help:
	$(CONT_FRONTOFFICE_NODE) $(MAKE_FULL) APP_ENV=$(APP_ENV) APP_CI=$(APP_CI) APP_DOMAIN=$(APP_DOMAIN) help

## Run in command in frontoffice
frontoffice-%:
	$(CONT_FRONTOFFICE_NODE) $(MAKE_FULL) APP_ENV=$(APP_ENV) APP_CI=$(APP_CI) $*

## Connect to frontoffice node container
frontoffice-sh:
	$(CONT_FRONTOFFICE_NODE) sh

## Run yarn command in frontoffice
frontoffice-yarn-%:
	$(CONT_FRONTOFFICE_NODE) $(MAKE_FULL) APP_ENV=$(APP_ENV) APP_CI=$(APP_CI) yarn-$*

## Run yarn command in frontoffice container, example: make api-composer c="install"
frontoffice-yarn:
	@$($(BIN_EVAL) c ?=)
	$(CONT_FRONTOFFICE_NODE) $(MAKE_FULL) APP_ENV=$(APP_ENV) APP_CI=$(APP_CI) yarn c="$(c)"

## Run ng command in frontoffice
frontoffice-ng-%:
	$(CONT_FRONTOFFICE_NODE) $(MAKE_FULL) APP_ENV=$(APP_ENV) APP_CI=$(APP_CI) ng-$*

## Run ng command in fontoffice container, example: make api-composer c="build"
frontoffice-ng:
	@$($(BIN_EVAL) c ?=)
	$(CONT_FRONTOFFICE_NODE) $(MAKE_FULL) APP_ENV=$(APP_ENV) APP_CI=$(APP_CI)  ng c="$(c)"

## Fix linux permissions into frontoffice container
frontoffice-permfix:
	$(CONT_FRONTOFFICE_NODE) $(MAKE_FULL) APP_ENV=$(APP_ENV) APP_CI=$(APP_CI) permfix uid=$(UID) gid=$(GID)

## Cleaning previous api install
frontoffice-clean:
	$(CONT_FRONTOFFICE_NODE) $(MAKE_FULL) APP_ENV=$(APP_ENV) APP_CI=$(APP_CI) clean

## Install frontoffice environment from scratch
frontoffice-install: frontoffice-yarn-install
	@$(BIN_ECHO) "${GREEN}FRONTOFFICE NodeJS vendors installed${RESET}"

## Install frontoffice environment from scratch with debug and tools
frontoffice-install-dev: frontoffice-yarn-install-dev
	@$(BIN_ECHO) "${GREEN}FRONTOFFICE NodeJS vendors tools installed${RESET}"

## Run qa tools on frontoffice container
frontoffice-qa:
	@$(BIN_ECHO) ''



#################################
Deploy:

## Deploy prod
deploy-prod:
	@$(BIN_ECHO) ''

## Deploy preprod
deploy-preprod:
	@$(BIN_ECHO) ''



#################################
QA:

## Run all qa tools
qa: api-qa-full frontoffice-qa-full qa-docker

## Run qa tool hadolint for dockerfiles
qa-docker:
	@for file in $$(find docker -name Dockerfile); \
  		do $(BIN_ECHO) "Lint : $(GREEN)$$file$(RESET)";  \
  		$(BIN_DOCKER) run --rm --mount type=bind,source=./docker/,target=/docker -i hadolint/hadolint hadolint /$$file --ignore="DL3018"; \
  done


#################################
Tests:

## Run all tests
tests-all: tests-unit tests-e2e

## Run phpunit tests
tests-unit: api-tests-unit frontoffice-tests-unit

## Run e2E tests
tests-e2e: api-tests-e2e frontoffice-tests-e2e



#################################
Documentation:

## Build documentations
doc-build: api-phpdocumentor api-nelmio frontoffice-compodoc
