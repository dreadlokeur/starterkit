# STARTER-KIT

## Requirements

* [docker](https://docs.docker.com/engine/install/) (version 19.0.3 minimum)
* [docker-compose](https://docs.docker.com/compose/install/) (version 2.20 minimum)
* [make](https://www.gnu.org/software/make/)
* [awk](https://www.funix.org/fr/unix/awk.htm) (optional)
* [openssl](https://www.openssl.org/) (optional)

## On the first, install the project

Install the project with following commands:

```bash
git clone git@gitlab.com:dreadlokeur/starterkit.git
cd starter-kit && make install
```

## Run the project

Start the project with following commands:

```bash
make start
```
If you want watching rebuilding nodes sources, run with following commands:

```bash
make watch-assets
```

## You need test the project ?

Run all test:

```bash
make tests-all
```

Run only unit test:

```bash
make tests-unit
```

Run only e2e test:

```bash
make tests-e2e
```

Run only qa test:

```bash
make qa
```


## Now, do you want to deploy your work ?

Run the command:

```bash
make deploy
```

## More information ?

Some help:

```bash
make help
```
Some help for the api:

```bash
make api-help
```
Some help for the frontoffice:

```bash
make frontoffice-help
```
